
configuracion basica router
R6(config)#no ip domain-lookup
R6(config)#banner motd #bienvenido#
R6(config)#line vty 0 3
R6(config-line)#password 1234
R6(config-line)#login
R6(config-line)#password FR
R6(config-line)#login
R6(config-line)#esit

configuracion switch capa 3
ESW1 # config t 
ESW1 (config) #hostname Switch1 
Switch1 (config) # exit 
Switch1 # vlan database 
Switch1 (vlan) # exit 
Switch1 # show vlan database

configuracion routers wan
FR(config)#int s1/0
FR(config-if)#ip add 10.10.1.18 255.255.255.252
FR(config-if)#clock rate 64000
FR(config-if)#no sh

rip v2

FR(config)#router rip
FR(config-router)#version 2
FR(config-router)#network 10.10.1.16
FR(config-router)#network 10.10.1.20
FR(config-router)#no auto-summary
FR(config-router)#end

DCE#show ip rip database
10.0.0.0/8    auto-summary
10.10.1.12/30
    [3] via 10.10.1.25, 00:00:09, Serial1/0
10.10.1.16/30
    [2] via 10.10.1.25, 00:00:09, Serial1/0
10.10.1.20/30
    [1] via 10.10.1.25, 00:00:09, Serial1/0
10.10.1.24/30    directly connected, Serial1/0

PPP
ISP-BOG(config)#username R1-BOG password 1234
ISP-BOG(config)#int s1/0
ISP-BOG(config-if)#encapsulation ppp
ISP-BOG(config-if)#ppp
ISP-BOG(config-if)#ppp authentication chap
ISP-BOG(config-if)#exit
ISP-BOG(config)#wr

hdlc 
ISP-ESP#conf t
ISP-ESP(config)#int s1/0
ISP-ESP(config-if)#encapsulation hdlc
ISP-ESP(config-if)#exit

frame relay
FR(config)#int s 1/0
FR(config-if)#encapsulation frame-relay
FR(config-if)#frame-relay route 107 interfase serial 1/0 701

server 
conf t

ip http server

ip http path flash:

ip http port 8000

ip http access-class 1

access-list 1 permit 192.168.20.0 255.255.255.0


dir flash:
copy http://192.168.20.2:8000/http-confg null:




